const TestRPC = require('ethereumjs-testrpc');
const Web3 = require('web3');

const MiniMeToken = require('./index.js').MiniMeToken;
const MiniMeTokenFactory = require('./index.js').MiniMeTokenFactory;
const MiniMeTokenState = require('./index.js').MiniMeTokenState;

const { utils } = Web3;

const verbose = true;

const log = (S) => {
    if (verbose) {
        console.log(S);
    }
};

/* let testrpc; */
let web3;
let accounts;
let miniMeToken;
let miniMeTokenState;
let miniMeTokenClone;
let miniMeTokenCloneState;
const b = [];

async function constructor() {
    testrpc = TestRPC.server({
        ws: true,
        gasLimit: 5800000,
        total_accounts: 10,
      });
  
    //  testrpc.listen(8547, '127.0.0.1');
    web3 = new Web3('http://localhost:8545');

  /*   web3 = new Web3(new 
        Web3.providers.HttpProvider("http://localhost:8545"));
   */  console.log('aqui');
    // setear las cuentas
    accounts = await web3.eth.getAccounts();
    // desbloquea la cuenta porque usando geth,
    // viene bloqueada
    /* mehor hacerlo por consola!
    web3.eth.personal.unlockAccount(accounts[0], 'prueba', 3600);
    web3.eth.personal.unlockAccount(accounts[1], 'prueba', 2600); */

  // await miniMeToken.generateTokens(accounts[0], 1000);
  // web3.fromWei(eth.getBalance(eth.coinbase), "ether");
    let balanace = await web3.eth.getBalance(accounts[0]);
    
    console.log('BALNACE!!!!' , utils.fromWei(balanace, 'ether'));

    // web3.eth.personal.unlockAccount(accounts[1], 'farsa'); */
    console.log('CCC', accounts);
    
    // log(accounts);
    // return accounts;

        var result = await web3.eth.estimateGas({
        to: "0xb8CbbAC8fEa82C7706b43B5318dCf94721d94f35", 
        data: "0xc6888fa10000000000000000000000000000000000000000000000000000000000000003"
    });
    console.log('res', result);

        // setear miniMeToken
        const tokenFactory = await MiniMeTokenFactory.new(web3);
    log('1');
    miniMeToken = await MiniMeToken.new(web3,
        tokenFactory.$address,
        0,
        0,
        'MiniMe Test Token',
        18,
        'MMT',
        true);
        
      //  await miniMeToken.generateTokens(accounts[0], 1000);
        // setear el estado de minimeToken (minimeTokenState)
        miniMeTokenState = new MiniMeTokenState(miniMeToken);

        return true;
    }
    
    
    async function createToken() {
        const miniMeTokenCloneTx = await miniMeToken.createCloneToken(
            'Clone Token 1',
        18,
        'MMTc',
        0,
        true);
        let addr = miniMeTokenCloneTx.events.NewCloneToken.raw.topics[1];
        console.log('pure cloned address', addr);
        addr = `0x${addr.slice(26)}`;
        addr = utils.toChecksumAddress(addr);
        console.log('pure cloned normalized token', addr);
      miniMeTokenClone = new MiniMeToken(web3, addr);
      miniMeTokenCloneState = new MiniMeTokenState(miniMeTokenClone);

      b[5] = await web3.eth.getBlockNumber();
    
      const j = await  web3.eth.getBlockNumber();
      console.log('prueba', j)
      // log(`b[5]->  ${b}`);

      const st = await miniMeTokenCloneState.getState();
      console.log('status', b);
      const totalSupply = await miniMeTokenClone.totalSupplyAt(b[5]);
      // console.log('totalSupplyAt', totalSupply);
      const balance = await miniMeTokenClone.balanceOfAt(accounts[2], b[5]);
      // console.log('balance', balance);
}

async function generateTokenForAddress1() {
  /*   b[0] = await web3.eth.getBlockNumber();
     log(`b[0]-> ${b[0]}`); */

    await miniMeToken.generateTokens(accounts[0], 1000);
    const st = await miniMeTokenState.getState();
   // b[1] = await web3.eth.getBlockNumber();
   /*  console.log('accounts', accounts[1]);
    console.log('st', st.balances[accounts[1]]); */
    // const balance = await miniMeTokenClone.totalSupplyAt(accounts[1]);
    // const balance = await miniMeTokenClone.balanceOfAt(accounts[1], 1);
    console.log('Token generated for account1', st.balances[accounts[1]]);
    
}

async function transferFromAddress1To2() {
    
    await miniMeToken.transfer(accounts[1], 2, { from: accounts[0], gas: 200000 });
    b[2] = await web3.eth.getBlockNumber();
    log(`b[2]->  ${b[2]}`);
    const st = await miniMeTokenState.getState();
   
    console.log('New status. Address 1: ', st.balances[accounts[0]]);
    console.log('----------> Address 2: ', st.balances[accounts[1]]);
    /* const balance = await miniMeToken.balanceOfAt(accounts[1], b[1]); */
}

constructor().then((a) => {
    // console.log('finish', accounts);
   createToken().then((b) => {
        // console.log('createToken', b);
        generateTokenForAddress1().then(c => {
            // console.log('generateTokenForAddress1', b);
            transferFromAddress1To2().then(c => {
                console.log('generateTokenForAddress1', b);
            })
        })
    })
});
